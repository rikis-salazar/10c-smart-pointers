#include <iostream>
#include "SmartPtr.h"

// Alias(es)
using std::cout;



// Forward declaration(s)
class Cosa;   // <-- Cosa == Thing in Spanish
void imprimeEstaCosa( SmartPtr<Cosa> p );   // <-- printThisThing() 



// "Here's the thing..." [literaly]
// Note that in order to use Cosa as a template param for our
// SmartPtr class, it has to provide a reference count function.
// In this case, we simply derived from a class that already 
// implements such a function.
class Cosa : public ReferenceCountedObject {
    private:
        // Keeps other 'Cosas' alive as long as this 'Cosa' exists. 
        SmartPtr<Cosa> ptr;   

        // Something to keep track of.
        int id;

        // Keeps track of how many cosas we create.
        // static int count = 0;  // needs to be initialized outside of the class
        static int count;  

    public:
        // Every Cosa is different. id will tell them apart.
        Cosa() : id(++count) { }

        // Let us know when Cosas die
        ~Cosa() { cout << "Killing thing " << id << "\n"; }

        // Let this Cosa point to other Cosa
        void set_smartPtr( SmartPtr<Cosa> p ){ this->ptr = p; }

        // Accessor(s)
        int get_id() const { return id; }
        void get_info() const {
            cout << "Cosa " << id << ", pointing to ";
            if ( ptr )
                cout << "Cosa " << ptr->get_id();
            else
                cout << "nobody";
            
            cout << ".\n";
            return;
        }


        // Manually reset internal SmartPtr
        void reset_internal_smartPtr(){ ptr.reset(); }


        // Inside printing job (illustrates handling of SmartPtrs)
        void inside_print_job(){
            // Get a SmartPtr object that shares ownership
            // with other SmartPtr's
            SmartPtr<Cosa> p(this);
            imprimeEstaCosa(p);

            // Shorter version
            // imprimeEstaCosa( SmartPtr<Cosa>(this) );

            // Even shorter
            // imprimeEstaCosa(this);
        }

};

// Static fields need to be initialized outside of the interface
int Cosa::count = 0;



// Non members that calls get_info()
void imprimeEstaCosa( SmartPtr<Cosa> sm_ptr ){
    cout << "Imprimiendo esta Cosa: "; // <-- "Printing this Thing"
    sm_ptr->get_info();
    return;
}

int main(){

    SmartPtr<Cosa> thing1( new Cosa );
    SmartPtr<Cosa> thing2( new Cosa );

    // Create a cycle with these Cosas
    thing1->set_smartPtr(thing2);
    thing2->set_smartPtr(thing1);

    // Displaying info [Never leaves the class]
    thing1->get_info();
    thing2->get_info();

    // Displaying again via member that calls non-member
    thing1->inside_print_job();
    thing2->inside_print_job();

    // Break the cycle created above.
    // What would happen if we do not do this here?
    thing1->reset_internal_smartPtr();

    // Direct reset is available too! 
    // The thing being reset dies immediately!
    thing2.reset();


    cout << "Leaving main function: Adios!!!\n";
    return 0;

    // Only thing left to do is to kill the thing ...
}


