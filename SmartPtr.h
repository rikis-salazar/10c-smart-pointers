/** Our very own attempt at implementing a
 * smart pointer.
 *
 * NOTE: The use of this class is strictly
 *       discouraged. C++ provides better
 *       tools in the <memory> library. 
 */

#ifndef ___Pic10C_Smart_Ptr___
#define ___Pic10C_Smart_Ptr___

class ReferenceCountedObject{
    private:
        // mutable??? what is this mutable thingy???
        mutable unsigned int reference_count;

    public:
        // ReferenceCountedObjects [constructors]
        // Their only job is to set the counter.
        ReferenceCountedObject() 
        : reference_count(0) {}

        ReferenceCountedObject( const ReferenceCountedObject& ) 
        : reference_count(0) {}

        // Abstract class. This is just intended to provide
        // a reference counting member function.
        virtual ~ReferenceCountedObject() {}


        // The only members that 'count' [pun intended].
        void increment_reference_count() const { 
            ++reference_count; 
        }

        void decrement_reference_count() const { 
            if (--reference_count == 0 ) 
                delete this; // <-- "Adios mundo cruel"
        }

        // Optional: Provide a way to determine number of shared pointer
        //           Useful for debugging purposes.
        unsigned int get_reference_count() const {
            return reference_count;
        }
};



// Let us focus first on the interface and write down 
// the  list of features that we want for our class
template <typename T>
class SmartPtr{
    private:
        T* thePtr;

    public:
        // Constructor: 
        // The nullptr default will be used
        // when we 'reset' the pointer.
        explicit SmartPtr( T* regularPtr = nullptr )
        : thePtr(regularPtr) {
            if ( thePtr ) 
                thePtr->increment_reference_count();
        }
        // NOTE(S): 
        // 0) 'explicit' added to the declaration. This disables the
        //    implicit conversion from a raw pointer to a SmartPtr.
        // 1) The user should stick to the convention that the 
        //    only way to use this constructor is via a new.
        //    statement passed as a a parameter.
        // E.g.   
        //       SmartPtr<Fraction> orig_p( new Fraction(1,2) );
        //    If the allocation of memory is succesfull, the 
        //    reference counter is increased from 0 to 1. If the
        //    allocation is not succesful the counter remains at 0.
        //    
        // 2) We are allowing the constructor to receive no 
        //    parameters. This is also intentional and it will
        //    be used later in the reset() function. In this case
        //    the counter does not change (remains at 0).
        //
        // SIDE EFFECT(S):
        //  - Reference count is 1 if memory alloction is 
        //    successful


        // Copy Constructor 
        SmartPtr( const SmartPtr& b ) 
        : thePtr(b.thePtr) {
            if ( thePtr ) 
                thePtr->increment_reference_count();
        }
        // NOTE(S): 
        // 1) There is an intended side effect here as well. 
        //    The statement
        //        SmartPtr<Fraction> other_p( orig_p );
        //    should reflect the fact that there is a new 
        //    reference to the original object.
        //
        // SIDE EFFECT(S):
        //  - Reference count increases 


        /** 
         *  We'll include yet one more constructor here 
         *  to allow for the implicit conversion between
         *  other SmartPtr types. 
         */
        template <typename S>
        SmartPtr( const SmartPtr<S>& b )
        : thePtr(b.thePtr) {
            if ( thePtr )
                thePtr->increment_reference_count();
        }
        // NOTE(S): 
        // 1) Conversions from a raw pointer to a SmartPtr
        //    are not allowed (see 'explicit' in default costr).
        // 2) Conversions between SmartPtr objects are OK.
        //
        // SIDE EFFECT(S):
        //  - Reference count increases 


        // Destructor
        ~SmartPtr(){
            if ( thePtr )
                thePtr->decrement_reference_count();
        }
        // NOTE(S): 
        // 1) When the SmartPtr goes out of scope it should
        //    reflect the fact that there is one less reference
        //    to the original object.
        //
        // SIDE EFFECT(S):
        //  - Reference count decreases 


        // Copy and Swap assignment operator. 
        const SmartPtr<T>& operator=( const SmartPtr<T>& rhs ){ 
            SmartPtr<T> rhsCopy(rhs);
            this->swap(rhsCopy);
            return *this;
        }
        // NOTE(S): 
        // 1) This is the regular copy and swap we know but...
        //    it comes with side effects.
        //
        // SIDE EFFECT(S):
        //  Consider the statement: lhs = rhs;
        //  - During creation, rhsCopy increases the ref count of rhs.
        //  After the swap rhsCopy is associated with *this ( i.e. lhs)
        //  - During destruction, rhsCopy decreases the ref count of lhs.


        // Reset function: Needed to 'manually' solve the cycle problem
        void reset(){ 
            SmartPtr<T> temp;
            this->swap(temp);
            return;
        } 
        // NOTE(S): 
        // 1) We ARE NOT following our own convention, namely that
        //    constructor receivs new as parameter. This is intentional!
        // 2) Copy and swap comes with side effects.
        //
        // SIDE EFFECT(S):
        //  - During creation the ref counter for temp is set to 0
        //  After the swap *this is associeted with temp.
        //  - During destruction, temp cuases the old ref count to 
        //    be lost.


        // Member swap (eliminates dependence on other libraries)
        void swap( SmartPtr<T>& other ){
            T* temp = thePtr;
            thePtr = other.thePtr;
            other.thePtr = temp;
            return;
        }
        // NOTE(S): 
        // 1) The same thing is achieved with the statement
        //        std::swap(thePtr,other.thePtr);
        //    but it requieres the library <algorithm>


        /** 
         *  We want our SmartPtr class to behave like a pointer.
         *  Thus, we need to be able to 'star it' and use the
         *  arrow opoerator. We also provide a way to get the
         *  to the 'dumb' pointer (dangerous).
         */

        // Accessor
        T* get() const { return thePtr; } 

        // De-reference operator
        T& operator*() const { return *thePtr; }

        // Arrow operator
        T* operator->() const { return thePtr; }


        /**
         *  Boolean operators. We'll see later that the ones that 
         *  occur the most (e.g., in <algorithm>) are ==, and <.
        */
        // Two SmartPtrs are equal if the underlying pointers match ...
        bool operator==( const SmartPtr<T>& b ){ 
            return thePtr == b.thePtr;
        }

        bool operator!=( const SmartPtr<T>& b ){ 
            return thePtr != b.thePtr;
        }

        // ... it is the same with operator< 
        bool operator<( const SmartPtr<T>& b ){ 
            return thePtr < b.thePtr;
        }

        // One more conversion. From SmartPtr<> to bool.
        // Why? To be able to write
        //    if ( smrtP ) std::cout << "Valid Smart Pointer\n";
        operator bool() const { return thePtr; }

};


/**
 *  Lastly, we add 'support' for static and dynamic casting.
 *  Just as before, the operation takes place on the underlying
 *  raw pointer.
 *
 *  Usage:
 *      smartP_to_t = static_SmartPtr_cast(smrtP_from_t);
 *      smartP_to_t = dynamic_SmartPtr_cast(smrtP_from_t);
 *
 *  Alternatively:
 *      sm_p = static_SmartPtr_cast<FROM_T,TO_T>(sm_p_from);
 *      sm_p = dynamic_SmartPtr_cast<FROM_T,TO_T>(sm_p_from);
 *
 */
template<typename FROM_T, typename TO_T>
SmartPtr<TO_T> static_SmartPtr_cast( const SmartPtr<FROM_T>& smrtP_from ){
    // Step 1. Extract raw pointer
    FROM_T* rawP_from = smrtP_from.get();

    // Step 2. Perform static cast of raw_pointer
    TO_T* rawP_to = static_cast<TO_T*>(rawP_from);

    // Step 3. Use the 'conversion constr' to wrap the casted raw pointer 
    SmartPtr<TO_T> smrtP_to(rawP_to);

    // Step 4. Return the newly created object.
    return smrtP_to;
}

// NOTE(S)
// 1) Same result is acieved with the statement
//        return SmartPtr<TO_T>( static_cast<TO_T*>(smrt_p_from.get()) ) ;
// 2) Once again, we failed to follow our own convention. 
//    Is this OK? Why?


template<typename FROM_T, typename TO_T>
SmartPtr<TO_T> dynamic_SmartPtr_cast( const SmartPtr<FROM_T>& smrtP_from ){
    return SmartPtr<TO_T>( dynamic_cast<TO_T*>(smrtP_from.get()) ) ;
}



#endif
