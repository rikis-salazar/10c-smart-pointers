/* 
 * Demo of how a shared pointer works in various situations.
*/

#include <memory>
#include <iostream>

// Alias(es)
using std::cout;
using std::shared_ptr;
using std::enable_shared_from_this;


// Forward declaration(s)
class Cosa;
void imprimeEstaCosa ( shared_ptr<Cosa> c );


// Simplified Cosas. All members are public.
// 
// To elimitate the double deletion problems we should keep the main
// manager alive [somehow]. After reviewing the concept of WEAK 
// pointers, hopefully ot makes sense to use a solution that implements
// this idea. C++ provides the template class
//     std::enable_shared_from_this
// that solves our problem. The only thing we need to do now is
// to modify our original Cosa class.
//
struct Cosa : public enable_shared_from_this<Cosa> {
    // Same fields as in our previous demo
    static int count;
    unsigned int id;
    shared_ptr<Cosa> ptr;

    // Same [or similar] functions as in our previous demo
    Cosa() : id(++count) { }
    ~Cosa() { cout << "Killing thing " << id << ".\n"; }

    void set_shared_ptr( shared_ptr<Cosa> p ){ this->ptr = p; }

    int get_id() const { return id; }

    void get_info() const {
        cout << "Cosa " << id << ", pointing to ";
        if ( ptr )
            cout << "Cosa " << ptr->get_id();
        else
            cout << "nobody";
        
        cout << ".\n";
        return;
    }


    // It works again... but as before we are using inheritance
    // Althought it seems that we went back to our previous
    // approach, the standard smart pointers have been widely
    // used and tested and most of the time they will provide
    // a better solution to a home-made implementation.
    void inside_print_job(){
        // Once again there will be two managers, but the original
        // one will not be killed by this member function.
        shared_ptr<Cosa> p = shared_from_this();
        imprimeEstaCosa(p);

        // Shorter version
        // imprimeEstaCosa( shared_from_this() );
    }
};

int Cosa::count = 0;


void imprimeEstaCosa( shared_ptr<Cosa> sh_ptr ){
    cout << "Imprimiendo esta Cosa: ";
    sh_ptr->get_info();
    return;
}


// As in the previous demo, we create Cosas that point to 
// each other to see how these smart pointer behave.

int main() {
    // Recall that a lot of thinking went into the design of
    // these smart pointer, however, they are not 100% 
    // foolproof. You still need to program by convention.
    // In particular, you should always use the following form
    shared_ptr<Cosa> c1(new Cosa);
    shared_ptr<Cosa> c2(new Cosa);

    /**
        // NOTE:
        // It is not possible to initialize a shared pointer using 
        // the assignment operator
	shared_ptr<Cosa> c3;
	Cosa* raw_ptr = new Cosa;
        c3 = raw_ptr;   // <-- Error! operator= was 'deleted'

        // NOTE: One can trick c3 [shared pointer] into accepting
        // raw_pointer, but we shouldn't do it unless we are 100%
        // sure raw_pointer is not used elsewhere.
	c3.reset(raw_ptr);
    */

    // Create cycle 
    c1->set_shared_ptr(c2);
    c2->set_shared_ptr(c1); // Comment/uncomment then compare outputs

    // display what each object is pointing to:
    c1->get_info();
    c2->get_info();

    // display again via member that calls non-member 
    c1->inside_print_job();
    c2->inside_print_job();
    // Oops!!! c1 got killed inside 'inside_print_job'. Why???

    // Use reset to break cycles.
    // c1.reset();
    // c2.reset();
    //
    // Note: Pay attention to the timing of the call to the destructors
    // What happens if there is no cycle and you
    // - reset c2 but not c1?
    // - reset c1 but not c2?
    // - reset both?
    // - do not reset any of them?
    //
    // Answer the same questions for the case when there is a cycle.
	
    cout << "Leaving main.\n";
}
