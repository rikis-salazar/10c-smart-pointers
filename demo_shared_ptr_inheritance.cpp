/**
 * In this demo we show examples of valid and invalid 
 * conversions between shared_ptr objects.
 */

#include <iostream>
#include <memory>

// Alias(es)
using std::cout;
using std::shared_ptr;
using std::static_pointer_cast;
using std::dynamic_pointer_cast;


// A couple of Base classes
class Base1 {
    private:
	static int count;
	unsigned int id;

    public:
        Base1() : id(++count) { }

        // NOTE: Because shared_ptr stores the original pointer type,
        // derived class objects will still get correctly destroyed
        // through shared_ptr<Base1> even if ~Base() is not declared 
        // virtual. However, declaring ~Base() as virtual should 
        // is still encouraged. 
        virtual ~Base1() {
            cout << "Killing Base1 object with id:" << id << ".\n";
        }

	int get_id() const { return id; }

	virtual void print() const {
            cout << "This is the " << id << "-th instance of Base1.\n";
            return;
        }
};

int Base1::count = 0;


class Base2 {
    private:
	static int count;
	unsigned int id;

    public:
        Base2() : id(++count) { }

        virtual ~Base2() {
            cout << "Killing Base2 object with id: " << id << ".\n";
        }

	int get_id() const { return id; }

	virtual void print() const {
            cout << "This is the " << id << "-th instance of Base2.\n";
            return;
        }
};

int Base2::count = 0;



// Some Derived classes. Since they already have a unique id,
// we assign another [arbitrary form of] identification.
class Derived1 : public Base1 {
    private:
	unsigned int sid;   // sid == sub id

    public:
        Derived1(unsigned int n) : sid(n) { }

	~Derived1() {
            cout << "Killing Derived1 object with sid: " << sid << ".\n";
        }

	virtual void print() const {
            cout << "Derived1 object. Full id: " 
                 << get_id() << "-" << sid << ".\n";
            return;
        }
};

class Derived2 : public Base1 {
    private:
	unsigned int sid;

    public:
        Derived2(unsigned int n) : sid(n) { }

	~Derived2() {
            cout << "Killing Derived2 object with sid: " << sid << ".\n";
        }

	virtual void print() const {
            cout << "Derived2 object. Full id: " 
                 << get_id() << "-" << sid << ".\n";
            return;
        }
};



// And finally another derived class that inherits from both Bases
class Derived3 : public Base1, public Base2 {
    private:
        unsigned int sid;

    public:
	Derived3(unsigned int n) : sid(n) { }

	~Derived3() {
            cout << "Killing Derived3 object with id:" << sid << ".\n";
        }

	virtual void print() const {
            cout << "Derived3 object with full id: [" 
                 << Base1::get_id() <<"," << Base2::get_id() << "]" 
                 << ":" << sid << ".\n";
            return;
        }
};


int main() {
    shared_ptr<Derived1> dp1( new Derived1(10) );
    cout << "dp1: ";
    dp1->print();

    // Assignment: Derived to Base (OK) 
    shared_ptr<Base1> bp1 = dp1;	
    cout << "bp1: ";
    bp1->print();			// virtual function call

    // Downcast [static]: Base to Derived (OK but might not be invalid)
    shared_ptr<Derived1> dp2 = static_pointer_cast<Derived1>(bp1);
    cout << "dp2: ";
    dp2->print();

    // Construction: Base from Derived [shared] (c.f. Derived to Base)
    shared_ptr<Base1> bp2(dp1); 
    cout << "bp2: ";
    bp2->print();
    
    // Construction: Base from Derived [raw] 
    shared_ptr<Base1> bp3( new Derived1(20) ); 
    cout << "\nbp3: ";
    bp3->print();
    cout << "\t'" <<typeid(*bp3.get()).name() << "'" 
         << " is the typeid of bp3.\n";


    // Downcast [dynamic]: Base to Derived [check the state afterwards]
    shared_ptr<Derived1> dp3 = dynamic_pointer_cast<Derived1>(bp3);
    cout << "Attempting to cast bp3 to Derived1...\n";
    if ( dp3 ) {
        cout << "\tSuccess! dp3 valid.\ndp3: ";
        dp3->print();
    }
    else
        cout << "\tDynamic cast failed.\n";
    
    // Construction: Base from Derived [raw]
    shared_ptr<Base1> bp4( new Derived2(30) );
    cout << "\nbp4: ";
    bp4->print();
    cout << "\t'" <<typeid(*bp4.get()).name() << "'" 
         << " is the typeid of bp4.\n";

    // Invalid cast [dynamic]: Derived2 to Derived1 [check afterwards]
    shared_ptr<Derived1> dp4 = dynamic_pointer_cast<Derived1>(bp4);  
    cout << "Attempting to cast bp4 to Derived1...\n";
    if( dp4 )
            dp4->print();
    else
            cout << "\tDynamic cast failed. dp4 not valid.\n";

    // Construction: Base from Derived [raw]
    shared_ptr<Base1> b1p5( new Derived3(40) );
    cout << "\nb1p5: ";
    b1p5->print();

    // Construction: Base from Derived [raw]
    shared_ptr<Base2> b2p1( new Derived3(41) );
    cout << "b2p1: ";
    b2p1->print();

    // Construction: Base from Base [raw]
    shared_ptr<Base1> bp6( new Base1 );
    cout << "bp6: ";
    bp6->print();


    /** *******************************************************
        ******************************************************* */
    
    // ERROR: No matching constructor.
    // shared_ptr<Base2> b2p2(b1p5);
    // ERROR: No matching constructor.
    // shared_ptr<Base2> b2p2(b1p5.get()); 

    /** *******************************************************
    shared_ptr_inheritance_demo.cpp: In function ‘int main()’:
    shared_ptr_inheritance_demo.cpp:203:32: error: no matching function for call to ‘std::shared_ptr<Base2>::shared_ptr(std::shared_ptr<Base1>&)’
         shared_ptr<Base2> b2p2(b1p5);
                                ^
        ******************************************************* */

    cout << "\nLeaving main()...\n\n";
}
