/* 
 * Demo of how a weak pointer can solve a cycle problem.
 */
#include <iostream>
#include <memory>

// Alias(es)
using std::cout;
using std::weak_ptr;
using std::shared_ptr;


// Simplified Cosas. All members are public.
//
// To eliminate the cycle problem we replace the internal shared_ptr
// with a weak_ptr. Weak pointers observe objects but do not own them.
// This allows for a manager to release a resource even if it is still
// being observed.

struct Cosa {
    // Not quite the same as before. Points to other Cosas but does
    // not affect lifetime
    weak_ptr<Cosa> ptr;	

    // All other fields remain the same
    unsigned int id;
    static int count;


    // Same or similar functions...
    Cosa() : id(++count) { }

    ~Cosa () {
        cout << "Killing thing " << id << ".\n";
    }

    void set_weak_ptr( shared_ptr<Cosa> p ) { this->ptr = p; }

    int get_id() const { return id; }

    // Who is this Cosa now pointing to [if anybody]?
    void get_info() const {
        cout << "Cosa " << id << ", pointing to ";

        // Is the object still there?
        if( ptr.expired() )	
            cout << "nobody"; 
        else {
            // Weak pointers cannot be dereferenced. So, we need to
            // create a temporary shared pointer, making sure the 
            // Cosa object stays around long enough to look at it.
            shared_ptr<Cosa> p = ptr.lock();

            // Note: Here we know the pointer is not expired, right?
            // Then why do we check for expiration again?
            // The answer has to do with multi-threading.
            if( p )     // <-- another way to test for expiration
                cout <<  "Cosa " << p->get_id(); 
        } 

        cout << ".\n";
        return;
    }

};

int Cosa::count = 0;


int main() {
    // Stick to the convention and use new in the constructor
    shared_ptr<Cosa> p1(new Cosa);
    shared_ptr<Cosa> p2(new Cosa);
    
    // Ok, where are you Cosa's pointing at?
    p1->get_info();
    p2->get_info();

    // Create cycle 
    p1->set_weak_ptr(p2);
    p2->set_weak_ptr(p1);
    
    // How about now?
    p1->get_info();
    p2->get_info();

    // We can still reset.  Reset with no arguments discards the 
    // pointed-to object; it zeroes-out the internal pointer to 
    // the shared object; 
    // p1.reset();
    // p2->get_info();
    
    cout << "Leaving main.\n";
    // When p1 and p2 go out of scope, if they are the last pointers
    // referrring to the pointed-to objects, they will free them 
}
