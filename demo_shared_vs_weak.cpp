/* 
 * Comparison of shared_ptr and weak_ptr 
 */

#include <iostream>
#include <memory>

// Alias(es)
using std::cout;
using std::weak_ptr;
using std::shared_ptr;


// Cosas, cosas y mas cosas...
class Cosa {
    private:
        unsigned int id;
        static int count;

    public:
        Cosa() : id(++count) {
            cout << "Creating thing " << id << "...\n";
        }
        ~Cosa () {
            cout << "Killing thing " << id << ".\n";
        }
        int get_id() const { return id; }
};

int Cosa::count = 0;


// Create a Cosa, but return a weak_ptr to it 
// The shared Cosa gets destructed
weak_ptr<Cosa> return_weak() {
    shared_ptr<Cosa> p (new Cosa);
    cout << "Inside 'return_weak': Cosa " << p->get_id() << ".\n";

    weak_ptr<Cosa> wp = p;
    return wp;
}

// Create a Cosa and return a shared_ptr to it
shared_ptr<Cosa> return_shared() {
    shared_ptr<Cosa> p (new Cosa);
    cout << "Inside 'return_shared': Cosa " << p->get_id() << ".\n";

    return p;
}


// Check weak_ptr to see if Cosa is still there
void still_there(weak_ptr<Cosa> wp) {
    if ( wp.expired() )
        cout << "No :-(. It expired. ";
    else
        cout << "Yes ;-). Still here. ";

    shared_ptr<Cosa> p = wp.lock();
    if( !p )
        cout << "The Cosa is gone!\n";
    else
        cout << "It is Cosa " << p->get_id() << ".\n";
}	

int main() {
    shared_ptr<Cosa> p (new Cosa);
    
    cout << "In 'main': Cosa " << p->get_id() << ".\n";
    
    cout << "\n";
    weak_ptr<Cosa> wp1 = return_weak();
    still_there(wp1);
    
    cout << "\n";
    p = return_shared();
    weak_ptr<Cosa> wp2 = p ;
    still_there(wp2);
    
    return 0;
}
