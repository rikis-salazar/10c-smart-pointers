## Summary of [home-made] SmartPtr

Our `SmartPtr` class allows us to 

* _default construct_, _construct from a raw pointer_, and _convert_
between SmartPtr types [if the conversion of raw pointers is legal
(e.g. an upcast)]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SmartPtr( T* regularPtr = nullptr );
    SmartPtr( const SmartPtr<T>& b );
    template<typename S>
    SmartPtr( const SmartPtr<S>& b );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* assign from another SmartPtr of the same type.
  + For safety purposes, we can't assign from a raw pointer (this 
  makes it easy to program by convention).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SmartPtr<T>& operator=( const SmartPtr<T>& rhs );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* clear with reset() [the SmartPtr object _points to nothing_]

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    void reset();
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* dereference, get the raw pointer, and convert to bool

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    T& operator*() const;
    T* operator->() const;
    T* get() const; // Do really want it?!
    operator bool() const; // true if managed object exists.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* access the [member] object reference count 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    unsigned int ReferenceCountedObject::get_reference_count() const;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* use the most common comparison operators (the operation applies to
the underlying pointers).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    bool operator==( const SmartPtr<T>& b );
    bool operator!=( const SmartPtr<T>& b );
    bool operator<( const SmartPtr<T>& b );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

* convert to another legal type or apply a dynamic cast. The idea
is to perform the corresponding casts on the internal pointer.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    template<typename FROM_T, typename TO_T>
    SmartPtr<TO_T> static_SmartPtr_cast( const SmartPtr<FROM_T>& smrtP_from );
    template<typename FROM_T, typename TO_T>
    SmartPtr<TO_T> dynamic_SmartPtr_cast( const SmartPtr<FROM_T>& smrtP_from );
//usage:
//  SmartPtr<to_type> p = dynamic_SmartPtr_cast<to_type>(from_type_ptr);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
