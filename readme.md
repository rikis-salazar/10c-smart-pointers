## Smart pointers

We know that dealing with _raw_ pointers is dangerous. It could potentially
lead to memory leaks and exceptions if the programmer does not properly
handle them. To alleviate this, some programming languages feature a 
_garbage collector_ whose main job is to release memory that is no longer
being used. One of the first programming languages that implemented this
concept was **LISP**. C and C++, whose specialty is 'speed', do not
provide such a tool mainly because:

+ control over execution time must be 'handed over' to the garbage
  collector, and it could take a while to get control back.
+ the collector runs independently of the programming logic, kicking in
  usually when memory is running low.

### So, what does C++ do to _clean up after itself_?

C++98 didn't do much. There was an attempt to provide a _smart pointer_
(`auto_ptr`) that could prevent some mishandling of memory, but the
implementation ended up being a disaster (mainly because at that time _move
semantics_ were not available).

C++11 introduces several **smart pointers** that help, but do not 
completely eliminate the problem. These pointers are: 

- `unique_ptr<T>`,
- `shared_ptr<T>`, and
- `weak_ptr<T>`.

In order to use them one needs to `#include<memory>`, a _tell-tell_ sign
that they are not _built-in_ language constructs, but rather Classes.
One needs to **program by convention** (e.g., the RAII idiom) to get 
all of the benefits they provide.

Newer standards (e.g. C++14 and 17) call for the _improvement_ of the
`new` operator to provide garbage collecting features.

### OK but ... What are the ideas behind these _smart pointers_ of yours?

The basic concepts behind smart pointers are:

- **a template class** where the template type corresponds to the type 
  of the _pointed to_ object, and the private field is a pointer of that
  type. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
  template <typename T>
  class SmartPtr{
      private:
          T* ptr;
  };
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
- **overloaded operators** that allow the pointer to _behave_ like a real 
  pointer. Some of these operators are: `operator*()`, and `operator->()`.

- **access** to the internal pointer if needed (e.g., via a member 
  function). Needless to say that this could be quite dangerous. Some
  implementations DO NOT provide such access. Example of such member
  functions are: the _conversion operator_ (`operator T*()`), and
  a plain and regular _accessor_ (`get()`).


### I think I get it, but ... How do we _smart-point_?

  Here are some ideas:

* Via **strict ownership**:

  + Only one pointer can _own_ the object at a time.
  + Copy constructor and assignment operator are disabled to prevent
    multiple owners of the same object. Instead, statments like
    `strct_ownrshp_ptr<T> p(q)`, and
    `existing_strct_ptr = some_other_strct_ptr;`, call the 
    _move constructor_ and the _move assignment operator_, respectively.
  + When the owner goes out of scope (or is manually destroyed), it 
    calls `delete` on the object that it is pointing to.

* Via [an _intrusive_] **reference count**:
  + It works via inheritance from a class that accompanies the 
    `SmartPtr` template.
  + When created, each object gets a zero reference count. 
    - Every other pointer that points to the object increments the 
      reference count.
    - When a pointer is destroyed [or _redirected_], it decrements the
      reference count.
    - When the reference count reaches [or is manually set to] zero,
      the `SmartPrt` deletes the object it is pointing to.

* Via [a _non intrusive_] **reference count**:
  + It works via a separate template class. Objects are created with 
    a call to `new` in the constructor of the pointer.
  + In C++11, this corresponds to `shared_pointer<T>` in `<memory>`.

> _NOTE_: When counting references, the only thing that matters is whether
> or not the count is zero. With this in mind, another way to implement
> a smart pointer (`LinkedPtr`) is via an internal linked list. 
> Assigning a pointer to another causes it to be _spliced into_ the
> list, whereas assigning the pointer to something else causes it to
> be _spliced out_ of the list. Finally, when the list is empty the
> object that is being pointed to gets deleted.

